# Privacy policy for mobile application "Smutify"

This privacy policy governs your use of the software application “Smutify” (later referred as the/this “Application”) for Android™ mobile devices that was created by s8dev.

The Application is a mobile Android application intended to aid in automatic device sound volume management. 


## User Provided Information

The Application does not require any user registration. The Application does not require the user to provide any personal information. The Application does not store any information locally.

## Automatically Collected Information

The Application does not collect any information automatically.

## Children

Our Application does not address anyone under the age of 13. We do not knowingly collect personally identifiable information from anyone under the age of 13. We do not use the Application to knowingly solicit data from or market to children under the age of 13.

## Changes

This Privacy Policy may be updated from time to time for any reason.

## Your Consent

By using the Application, you only consent to the local processing of your information.

At any time, no data will be shared, processed and/or stored by third parties.

## Contact Us

If you have any questions regarding privacy while using the Application, or have questions about our practices, please contact us via email at 077741@gmail.com
